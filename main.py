import argparse

import src.scenarios as scenarios
from src.sim import Simulation, SimRunSettings
from src.utils.exceptions import IdException
from src.utils.helper import ConfigCWC, ConfigBundledLinear


def parse_args():
    parser = argparse.ArgumentParser(
        description='This script can be used for running simulations in a multiprocessing\n'
                    'environment. The results will be stored in csv file.'
    )
    parser.add_argument('-p', type=int, dest='p', default=13,
                        help='The prime number used for the base prime field')
    parser.add_argument('-k_i', type=int, dest='k_i', default=2,
                        help='Dimension of inner RS encoder (RS2).')
    parser.add_argument('-k_o', type=int, dest='k_o', default=13,
                        help='Dimension of outer RS encoder (RS2).')
    parser.add_argument('-k', type=int, dest='k', default=None,
                        help='Dimension RS encoder (RS).')
    parser.add_argument('-m', type=int, dest='m', default=None,
                        help='RM encoder parameter.')
    parser.add_argument('-r', type=int, dest='r', default=None,
                        help='RM encoder parameter.')
    parser.add_argument('--cwc', type=str, dest='config_cwc', default='ppm',
                        help='Configuration for CWC encoder.\n'
                             'Options:\n'
                             '  * ppm\n'
                             '  * ooc')
    parser.add_argument('--lin', type=str, dest='config_lin', default='rs2',
                        help='Configuration for outer linear encoder.\n'
                             'Options:\n'
                             '  * rs\n'
                             '  * rs2\n'
                             '  * rm\n'
                             '  * NoCode')
    parser.add_argument('--rounds', type=int, dest='rounds', default=100,
                        help='Transmission rounds per processing core')
    # scenarios
    parser.add_argument('--sanity-check', dest='sanity_check',
                        default=True, action='store_true',
                        help='Flag for running sanity check.')
    parser.add_argument('--random-pairs', dest='random_pairs',
                        default=False, action='store_true',
                        help='Flag for running random pairs scenario.')
    parser.add_argument('--deterministic-sub', dest='deterministic_sub',
                        default=False, action='store_true',
                        help='Flag for running deterministic sub scenario.')
    parser.add_argument('--deterministic-pairs', dest='deterministic_pairs',
                        default=False, action='store_true',
                        help='Flag for running deterministic pairs scenario.')
    # settings
    parser.add_argument('--sum-run-results', dest='sum_run_results',
                        default=False, action='store_true',
                        help='Specifies whether the results of every worker '
                             'should be summed up within a run.')
    parser.add_argument('--iterate-cues', dest='iterate_cues',
                        default=False, action='store_true',
                        help='Overwrites communication rounds to iterate over all'
                             'possible cues in codeword.')
    parser.add_argument('--id-tx-as-str', dest='id_as_str',
                        default=False, action='store_true',
                        help='Allows string transmitted and subscribed ID as string.')
    return parser.parse_args()


def main():
    args = parse_args()
    sim = Simulation(p=args.p,
                     config_cwc=ConfigCWC(str.upper(args.config_cwc)),
                     config_lin=ConfigBundledLinear(str.upper(args.config_lin)),
                     run_settings=SimRunSettings(
                         sum_run_res=args.sum_run_results,
                         iterate_cues=args.iterate_cues,
                         id_as_str=args.id_as_str,
                     ),
                     k=args.k,
                     k_i=args.k_i,
                     k_o=args.k_o,
                     m=args.m,
                     r=args.r)

    # make sure only one scenario flag is set
    arg_flags = iter([args.sanity_check,
                      args.random_pairs,
                      args.deterministic_sub,
                      args.deterministic_pairs])
    if not (any(arg_flags) and not any(arg_flags)):
        raise IdException('More than one scenario flag set.')

    if args.sanity_check:
        scenarios.sim_sanity_check(sim, args.rounds)
    elif args.random_pairs:
        scenarios.sim_random_pairs(sim, args.rounds)
    elif args.deterministic_sub:
        scenarios.sim_deterministic_sub(sim, args.rounds)
    elif args.deterministic_pairs:
        scenarios.sim_deterministic_pairs(sim, args.rounds)


if __name__ == '__main__':
    main()
