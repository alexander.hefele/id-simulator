from src.sim import Simulation, ScenarioFlags


def sim_sanity_check(sim: Simulation, rounds: int):
    num = sim.get_random_id()
    return sim.run(rounds, id_tx=num, id_sub=num)


def sim_random_pairs(sim: Simulation, rounds: int):
    sim.scenario_flags = ScenarioFlags(random_per_round=False)
    return sim.run(rounds)


def sim_deterministic_sub(sim: Simulation, rounds: int):
    return sim.run(rounds, id_tx=None, id_sub=sim.sink.subs[0])


def sim_deterministic_pairs(sim: Simulation, rounds: int):
    num = sim.get_random_id()
    return sim.run(rounds, id_tx=num, id_sub=sim.sink.subs[0])
