import itertools
import os
import time

import numpy as np
import pandas as pd

from src.encoder import BundledLinearEncoder, Code, BaseCWC
from src.utils.exceptions import IdCodeParameterException
from src.utils.helper import is_prime, ConfigBundledLinear, ConfigCWC, get_prime_sequence, PATH_DATA, RSExtension


class CodeEstimation:
    """
    Orchestrator class for parameter estimation.
    """
    PARAM_EST_RES_DIR = os.path.join(PATH_DATA, 'params')

    PANDAS_INT = np.uint64
    PANDAS_FALLBACK = np.nan

    def __init__(self, cwc_config: ConfigCWC, lin_config: ConfigBundledLinear, rs_ext: RSExtension = RSExtension.NONE):
        """
        Initializes object for parameter estimation on given code class.
        """
        self.cwc_config = cwc_config
        self.lin_config = lin_config
        self.rs_ext = rs_ext
        self.code_params = ['p', *BundledLinearEncoder.required_code_params(lin_config)]
        self.data = list()
        self.df = pd.DataFrame()

        if not os.path.exists(CodeEstimation.PARAM_EST_RES_DIR):
            os.makedirs(CodeEstimation.PARAM_EST_RES_DIR)

    def run(self, p: int or list,
            k: int or list = None,  # RS
            k_i: int or list = None, k_o: int or list = None,  # RS2
            r: int or list = None, m: int or list = None,  # RM
            save: bool = False):
        """
        Runs parameter estimation for given code class.

        The parameters can either be given as single value integer or as list of
        integers. Every calculated (rate, error exponent) tuple is added to the
        object's dataframe. Thus, this function can be called sequentially for
        different parameter configurations and the results will all be added to
        the same dataframe.

        :param p: prime
        :param k: dimension single RS encoder
        :param k_i: dimension inner RS encoder
        :param k_o: dimension outer RS encoder (only for CWCs)
        :param save: states if dataframe should be saved after calculating the parameter grid
        :param r: degree of RM code
        :param m: number of variables for RM code
        :return: object' dataframe
        """
        p = [p] if type(p) is int else p
        k = [k] if type(k) is int else k
        k_i = [k_i] if type(k_i) is int else k_i
        k_o = [k_o] if type(k_o) is int else k_o
        r = [r] if type(r) is int else r
        m = [m] if type(m) is int else m

        code_config = []
        if self.lin_config == ConfigBundledLinear.RS:
            code_config = itertools.product(p, k)
        if self.lin_config == ConfigBundledLinear.RS2:
            code_config = itertools.product(p, k_i, k_o)
        if self.lin_config == ConfigBundledLinear.RM:
            code_config = itertools.product(p, r, m)
        if self.lin_config == ConfigBundledLinear.NOCODE:
            code_config = itertools.product(p, k)
        code_config = [dict(zip(self.code_params, config)) for config in code_config]

        for config in code_config:
            if not is_prime(config['p']):
                raise IdCodeParameterException
            if not Code.verify_param_lin(self.lin_config, **config):
                # print('Parameter invalid: ', config)
                continue
            code = Code(config_cwc=self.cwc_config, config_lin=self.lin_config,
                        cwc_codebook=False, rs_ext=self.rs_ext, **config)
            code_specs = code.get_full_code_specs_dict()
            self.data.append(code_specs)
            # self.df = self.df.append(self.format_data(code_specs), ignore_index=True)
            tmp_df = pd.DataFrame([self.format_data(code_specs)])
            self.df = pd.concat([self.df, tmp_df], ignore_index=True)
        self.df = self.df.drop_duplicates(self.code_params)
        return self.save_df() if save else self.data

    def format_data(self, code_config: dict):
        for config_key, config_val in code_config.items():
            if config_key in self.code_params + list(BaseCWC.Params._fields):
                code_config[config_key] = CodeEstimation.__convert_to_pandas_int(config_val)
        return code_config

    def save_df(self, f_name: str = None):
        """
        Saves dataframe to default location with default filename if not stated
        otherwise in `f_name`.

        :param f_name: (optional) filename to be used.
        """
        if f_name is None:
            f_name = 'params_%s_%s.csv' % (str.lower(self.cwc_config.value),
                                           str.lower(self.lin_config.value))
        assert self.df.shape[0] > 0
        self.df.to_csv(os.path.join(CodeEstimation.PARAM_EST_RES_DIR, f_name))
        return self.df

    @staticmethod
    def __convert_to_pandas_int(val):
        if val <= np.iinfo(CodeEstimation.PANDAS_INT).max:
            return CodeEstimation.PANDAS_INT(val)
        return CodeEstimation.PANDAS_FALLBACK


def estimate(cwc_config: ConfigCWC, lin_config: ConfigBundledLinear, p_range: set = (1, 201),
             k_range: set = (2, 6),  # RS
             k_i_range: set = (2, 6), k_o_range: set = None,  # RS2
             r_range: set = (1, 20), m_range: set = (2, 6),  # RM
             include_delta: bool = True, save_df: bool = False):
    def status_print(p: int, _p: int, t_start):
        if _p != p:
            _p = p
            print("  >>  %f sec" % (time.time() - t_start))
            t_start = time.time()
            print('p = %d' % p, end='')
        return _p, t_start

    print('## Parameter estimation for [%s, %s] code ##' % (lin_config.value, cwc_config.value))
    est = CodeEstimation(cwc_config, lin_config)
    p_list = get_prime_sequence(*p_range)
    _p = p_list[0]
    t_start = time.time()
    print('p = %d' % p_list[0], end='')
    if lin_config == ConfigBundledLinear.RS:
        k_list = range(k_range[0], k_range[1] + 1)
        for (p, k) in itertools.product(p_list, k_list):
            _p, t_start = status_print(p, _p, t_start)
            est.run(p=p, k=k)
    if lin_config == ConfigBundledLinear.RS2:
        k_i_list = range(k_i_range[0], k_i_range[1] + 1)
        for (p, k_i) in itertools.product(p_list, k_i_list):
            _p, t_start = status_print(p, _p, t_start)
            if k_o_range is not None:
                k_o_list = range(k_o_range[0], k_o_range[1] + 1)
            else:
                k_o_list = np.append(np.arange(1, p, dtype=np.uint64),
                                     np.unique(np.logspace(start=1, stop=k_i, num=300, base=p, dtype=np.uint64)))
                k_o_list = list(map(int, k_o_list))
                if include_delta:
                    k_o_list += [p ** (k_i - delta) for delta in range(1, k_i)]
            est.run(p=p, k_i=k_i, k_o=k_o_list)
    if lin_config == ConfigBundledLinear.RM:
        r_list = range(r_range[0], r_range[1] + 1)
        m_list = range(m_range[0], m_range[1] + 1)
        for (p, r, m) in itertools.product(p_list, r_list, m_list):
            _p, t_start = status_print(p, _p, t_start)
            est.run(p=p, r=r, m=m)
    if lin_config == ConfigBundledLinear.NOCODE:
        raise Code
    print("  >>  %f sec" % (time.time() - t_start))
    return est.save_df() if save_df else est.data


if __name__ == '__main__':
    df = estimate(ConfigCWC.PPM, ConfigBundledLinear.RS, save_df=True)
    df = estimate(ConfigCWC.OOC, ConfigBundledLinear.RS, save_df=True)
    df = estimate(ConfigCWC.PPM, ConfigBundledLinear.RS2, save_df=True)
    df = estimate(ConfigCWC.OOC, ConfigBundledLinear.RS2, save_df=True)
    df = estimate(ConfigCWC.PPM, ConfigBundledLinear.RM, save_df=True)
    df = estimate(ConfigCWC.OOC, ConfigBundledLinear.RM, save_df=True)
