class IdException(Exception):
    """
    General purpose ID exception.
    """
    def __init__(self, message='General ID Exception'):
        self.message = message
        super().__init__(self.message)


class IdCodeException(Exception):
    """
    Raised if a combination of ID codes are incompatible.
    """
    def __int__(self, message='ID Codes incompatible'):
        self.message = message
        super().__init__(self.message)


class IdCodeParameterException(Exception):
    """
    Raised if ID code parameters are incompatible.
    """
    def __int__(self, message='ID Code Parameters incompatible'):
        self.message = message
        super().__init__(self.message)


class CueLengthException(Exception):
    """
    Raised for cues that are too long.
    """
    def __int__(self, message='Cue too long'):
        self.message = message
        super().__init__(self.message)

