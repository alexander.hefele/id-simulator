import enum
import os

import numpy as np
from sage.all import IntegerRing, GF, Integer
from sage.arith.misc import is_prime

Z = IntegerRing()
PATH_DATA = os.path.join('data')


def get_prime(n_bits: int):
    """
    Determines prime number that is closest (smaller) to the given amout of bits.

    :param n_bits: bits available for representing prime number
    :return: prime number
    """
    sub = {3: 1, 4: 3, 5: 1, 6: 3, 7: 1, 8: 5, 9: 3, 10: 3,
           11: 9, 12: 3, 13: 1, 14: 3, 15: 19, 16: 15}
    return 2**n_bits - sub[n_bits]


class ConfigCWC(str, enum.Enum):
    PPM = 'PPM'
    OOC = 'OOC'
    HCWC = 'HCWC'


class ConfigBundledLinear(str, enum.Enum):
    RS = 'RS'
    RS2 = 'RS2'
    RM = 'RM'
    NOCODE = 'NOCODE'


class RSExtension(enum.Enum):
    NONE = 0
    SINGLE = 1
    DOUBLE = 2


class ResultID(str, enum.Enum):
    """
    Enum for results of the evaluation of the hypothesis test.
    """
    HYP_NEG = 'hyp_neg'
    HYP_POS = 'hyp_pos'
    HYP_FALSE_POS = 'hyp_false_pos'
    HYP_FALSE_NEG = 'hyp_false_neg'


class FiniteField:
    """
    Class for finite field operations (using SageMath)
    """

    class FieldElRepr(enum.Enum):
        """
        Enum for different field element representations.

        Modes:
                - 'integer'        : a single integer
                - 'polynomial'     : a polynomial over the primitive element of F_{q^k}
                                     (!!! this is the only format that depends on k,
                                          therefore an input 'k' is provided to override it)
                - 'integer list'   : a list of integers modulo q
                - 'polynomial list': a list polynomials over the primitive element of F_{q}
                - 'basefield list' : a list of digits modulo p
        """
        INT = enum.auto()
        INT_LIST = enum.auto()
        POLYNOMIAL = enum.auto()
        POLYNOMIAL_LIST = enum.auto()
        BASEFIELD_LIST = enum.auto()

    def __init__(self, q: int, length: int):
        self.q = q
        self.len = length

        # GF(p^m) with primitive element a
        self.gf = GF(q, 'a', modulus='primitive')
        self.a = self.gf.primitive_element()  # primitive element
        self.p = self.gf.base().order()  # base of prime field
        self.m = self.gf.degree()  # degree of extension field
        assert self.len >= self.m

    def convert_number(self, number, informat: FieldElRepr, outformat: FieldElRepr, padding: bool = False):
        """
        Helper functions to convert a number in a finite field into various formats (representations).
        Let the field be F_{q^k} with q=p^m and p a prime.

        Conversion paths:
            integer <-> integer list <-> polynomial list <-> basefield list <-> polynomial

        Namely:
            integer         -> integer list
            polynomial      -> basefield list
            integer list    -> polynomial list, integer
            basefield list  -> polynomial list, polynomial
            polynomial list -> basefield list, integer list
            -> the other paths are obtained by recursion.

        [Python comment: the conversion paths make it impossible to implement this function
        as a call to a dictionary of functions convert[informat](number, outformat)]

        :param number: number to be converted in given informat
        :param informat: format of input data
        :param outformat: format of desired output
        :param padding: adds zero padding (default: false).
        :return: number in desired output format
        """
        if informat == outformat:
            return number

        if informat == FiniteField.FieldElRepr.INT:
            # Create digits modulo q and handle as an 'integer list'
            # In order to use digits() we need to change to IntegerRing() type
            return self.convert_number(
                self.__zero_pad(Z(number).digits(self.q), padding),
                FiniteField.FieldElRepr.INT_LIST,
                outformat
            )

        if informat == FiniteField.FieldElRepr.POLYNOMIAL:
            # Get the list of coefficients and handle as a 'basefield list'
            # The list must be padded because a polynomial of degree j
            # in a field of degree k>j only returns a list of j coefficients
            basefield_list = number.polynomial().list()
            # TODO small difference to RM conversion
            return self.convert_number(
                basefield_list + [0] * (-len(basefield_list) % self.m),
                FiniteField.FieldElRepr.BASEFIELD_LIST,
                outformat
            )

        if informat == FiniteField.FieldElRepr.INT_LIST:
            if outformat == FiniteField.FieldElRepr.INT:
                # TODO small difference to RM conversion
                return np.sum(np.array(number) * [self.q ** i for i in range(len(number))])

            # Convert the integers to polynomials {0,a^0,a^1,...,a^(q-2)}
            # (zeros must be treated differently)
            # then handle as a 'polynomial list'
            return self.convert_number(
                [self.a ** (i - 1) if i != 0 else self.gf(0) for i in number],
                FiniteField.FieldElRepr.POLYNOMIAL_LIST,
                outformat
            )
            # Alternative: numpy entrywise ** and masking
            # Problem: only with GF(n,impl="ntl") which works only for n=2^m
            # Solution: keep above until sage behaviour is well defined
            # Alternative code:
            #   mask = self.identity == 0
            #   self.identity = self.a**(self.identity - 1)
            #   self.identity[mask] = self.field(0)
            # The probability of having a zero element is smaller as q grows,
            # so the extra computation of a^-1 is not a burden

        if informat == FiniteField.FieldElRepr.BASEFIELD_LIST:
            if outformat == FiniteField.FieldElRepr.POLYNOMIAL:
                # The choice of primitive element needs a choice of k
                # if k==None: k = self.k
                # If we make this choice a parameter, it must be passed down in the recursion
                primitive_element = GF(self.q ** self.len, 'a', modulus='primitive').primitive_element()
                return np.sum(np.array(number) * [primitive_element ** i for i in range(len(number))])

            # Divide the list into chunks of size m (q=p^m)
            # and produce the list of polynomials F_q = F_{p^m}
            # then handle as a 'polynomial list'
            return self.convert_number(
                np.sum(np.array(number).reshape([-1, self.m]) * [self.a ** i for i in range(self.m)], 1),
                FiniteField.FieldElRepr.POLYNOMIAL_LIST,
                outformat
            )

        if informat == FiniteField.FieldElRepr.POLYNOMIAL_LIST:
            if outformat in (FiniteField.FieldElRepr.INT, FiniteField.FieldElRepr.INT_LIST):
                # Convert the polynomials to integers inverting {0,a^0,a^1,...,a^(q-2)}
                # (zeros must be treated differently)
                # then handle as an 'integer list'
                return self.convert_number(
                    [i.log(self.a) + 1 if i != 0 else 0 for i in number],
                    FiniteField.FieldElRepr.INT_LIST,
                    outformat
                )
            if outformat in (FiniteField.FieldElRepr.POLYNOMIAL, FiniteField.FieldElRepr.BASEFIELD_LIST):
                # Convert the polynomials to lists of coefficients
                number_lists = [p.polynomial().list() for p in number]
                # The lists of coefficients must be padded
                # Then flatten the list and handle as a 'basefield list'
                return self.convert_number(
                    [i for p in number_lists for i in p + [0] * (-len(p) % self.m)],
                    FiniteField.FieldElRepr.BASEFIELD_LIST,
                    outformat
                )
            raise ValueError('Invalid output format')

        raise ValueError('Invalid input format')

    def __zero_pad(self, list_in: list, padding: bool = True):
        """
        Zero-Padding for lists.

        ATTENTION: Lists are given in Little-Endian (-> LSB first)
        :param list_in: list
        :param padding: specifies if padding should be applied (default: True)
        :return: zero-padded list
        """
        if not padding:
            return list_in

        return list_in + [Integer(0)] * (self.len - len(list_in))


def get_prime_sequence(start: int, stop: int):
    """
    Computes prime sequence until given value.

    :param start: min value for prime sequence
    :param stop: max value for prime sequence
    :return: prime sequence
    """

    return [p for p in range(start, stop + 1) if is_prime(p)]
