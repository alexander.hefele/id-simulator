import abc
import collections
import math
import random

import numpy as np
from sage.all import is_prime

from src.utils.exceptions import CueLengthException, IdCodeParameterException, IdCodeException, IdException
from src.utils.helper import FiniteField, ConfigCWC, ConfigBundledLinear, RSExtension


class BaseBlockCode(abc.ABC):
    """
    This class is the base class for general block codes with a
    given dimension and length as fundamental code parameters.
    """

    @abc.abstractmethod
    def length(self):
        """
        :return: length of code (codeword vector length)
        """
        pass

    @abc.abstractmethod
    def dimension(self):
        """
        :return: dimension of code (message vector length)
        """
        pass


class BaseCWC(BaseBlockCode, abc.ABC):
    """
    This is the base class for a constant weight block code.

    A block code in this context is basically a look-up table with N rows
    and S columns where every row is a codeword that is associated with the
    message word.

    A CWC has 4 primary parameters:
        * S: column count of table
        * N: row count of table
        * W: amount of 1's in each row
        * K: amount of overlapping 1's in each row

    Based on the constant weight property of a CWC the distance of the code
    can be determined based on those parameters. Those parameters have to be
    set when instantiating a certain CWC in the child classes.
    """

    Params = collections.namedtuple('Params', ['S', 'N_base', 'N_exp', 'W', 'K'])
    params: Params

    def length(self):
        """
        :return: length of code (codeword vector length).
        """
        return self.params.S

    def dimension(self):
        """
        :return: dimension of code (message vector length).
        """
        return self.params.N_base ** self.params.N_exp

    def distance(self):
        """
        :return: code distance
        """
        return 2 * (self.params.W - self.params.K)  # lemma 1 in Günlü 2021


class BaseLinearBlockCode(BaseBlockCode, abc.ABC):
    """
    This is the base class for a linear block code.

    With the linearity property it is possible to determine
    codewords ad-hoc for a given message. Those encoding
    relations invoke a certain distance in the codeword set.

    The parameters used for linear block codes are generally:
        * n: length
        * k: dimension
        * d: distance

    Those parameters have to be determined when instantiating
    a code object in one of the child classes.
    """

    Params = collections.namedtuple('Params', ['n', 'k', 'd'])
    params: Params

    def length(self):
        """
        :return: code length (codeword vector length)
        """
        return self.params.n

    def dimension(self):
        """
        :return: code dimension (message vector length)
        """
        return self.params.k

    def distance(self):
        """
        :return: code distance
        """
        return self.params.d

    @staticmethod
    @abc.abstractmethod
    def verify_code_config():
        pass


class BaseEncoder(abc.ABC):
    """
    Base class for encoding functionality of a code.

    This class must be inherited when wanting to apply encoding
    to the constructed codes.
    """

    CodewordID = collections.namedtuple('Codeword', ['cue', 'symbol'])
    format_out = FiniteField.FieldElRepr.POLYNOMIAL

    @abc.abstractmethod
    def encode(self, symbols: list, cue: int, **kwargs):
        """
        Encoding function -> needs to be overwritten in child class.

        :param symbols: list of symbols that need to be transmitted
        :param cue: position of the codeword symbol that should be determined (linear code only)
        :param kwargs:
        :return: CodewordID (cue, symbol) tuple
        """
        pass

    @abc.abstractmethod
    def get_code_params(self):
        pass


class Code(BaseCWC):
    """
    Wrapper object for an ID code as an ID CWC.

    This class summarizes the different CWC and linear codes that are used
    for constructing the ID code as an ID-CWC. Most of the functionality in
    this class focuses on KPI determination, parameter sanity checks, etc.

    Due to memory and complexity constraints it is not possible to pre-determine
    the entire codebook of the concatenated ID CWC. The ID codeword must be
    determined ad-hoc iteratively. Therefore, this class does not have any
    encoding functionality.
    """

    def __init__(self, p: int, config_cwc: ConfigCWC, config_lin: ConfigBundledLinear,
                 cwc_codebook: bool = True, rs_ext: RSExtension = RSExtension.NONE, **param_lin):
        """
        :param p: symbol size
        :param config_cwc: CWC config
        :param config_lin: Linear block code config
        :param cwc_codebook: false if codebook should not be determined (for KPIs only)
        :param rs_ext: RS extension information
        :param param_lin: parameters of linear block code
        """
        self.p = p
        self.config_cwc = config_cwc
        if not Code.verify_param_lin(config_lin, p=p, **param_lin):
            raise IdCodeParameterException
        self.config_lin = config_lin
        self.rs_ext = rs_ext
        self.param_lin = param_lin
        k_i = param_lin['k_i'] if 'k_i' in param_lin.keys() else None
        self.encoder = [
            EncoderCWC(p, config_cwc, cwc_codebook, k_i=k_i),
            BundledLinearEncoder(p, config_lin, rs_ext, **param_lin)
        ]

        # determine CWC parameters with concatenation
        self.params = self.encoder[0].params
        if config_cwc == ConfigCWC.OOC or config_cwc == ConfigCWC.HCWC:
            if not config_lin == ConfigBundledLinear.RS2:
                raise IdCodeException
            del self.encoder[1].encoder[1]
        for enc in self.encoder[1].encoder[::-1]:
            self.params = self.__concat_lin_code(enc)

    def calc_rate(self):
        """
        :return: ID rate KPI
        """
        return (math.log2(self.params.N_exp * math.log2(self.params.N_base))
                / math.log2(self.params.S))

    def calc_e2(self):
        """
        :return: ID error exponent KPI
        """
        return -math.log2(self.calc_lambda2()) / math.log2(self.params.S)

    def calc_lambda2(self):
        """
        :return: ID error bound KPI
        """
        return self.params.K / self.params.W

    def get_code_specs_dict(self):
        """
        The code specs in this method include:
            * CWC config
            * Linear block code config
            * symbol size
            * parameters of linear block code

        :return: dictionary with code specs
        """
        return dict(
            config_cwc=self.config_cwc.value,
            config_lin=self.config_lin.value,
            p=self.p,
            **self.param_lin,
        )

    def get_full_code_specs_dict(self):
        """
        In to the code parameters this method provides the
        code KPIs.

        :return: dictionary with full code specs
        """
        return dict(
            rate=self.calc_rate(),
            e2=self.calc_e2(),
            lambda2=self.calc_lambda2(),
            rs_ext=self.rs_ext.value,
            n_cue=math.log2(self.length()),
            n_symbol=math.log2(self.p),
            n_info=self.params.N_exp * math.log2(self.params.N_base),
            **self.params._asdict(),
            **self.get_code_specs_dict()
        )

    def __concat_lin_code(self, lin_code: BaseLinearBlockCode):
        """
        Concatenates the CWC with the outer linear block code(s).

        The concatenation only focuses on the resulting parameters!

        :return: resulting CWC parameters
        """
        if self.config_cwc == ConfigCWC.OOC:
            return BaseCWC.Params(
                S=self.params.S * lin_code.length(),
                N_base=self.params.N_base,
                N_exp=self.params.N_exp * lin_code.dimension(),
                W=self.params.W * lin_code.length(),
                K=self.params.W * (lin_code.length() - lin_code.distance()) + self.params.K * lin_code.distance()
            )
        return BaseCWC.Params(
            S=self.params.S * lin_code.length(),
            N_base=self.params.N_base,
            N_exp=self.params.N_exp * lin_code.dimension(),
            W=self.params.W * lin_code.length(),
            K=self.params.W * (lin_code.length() - lin_code.distance()) + self.params.K * lin_code.length()
        )

    @staticmethod
    def verify_param_lin(config_lin, **config_code):
        """
        :return: true if parameters are valid
        """
        for param in BundledLinearEncoder.required_code_params(config_lin):
            if param not in config_code.keys() or config_code[param] is None:
                return False
        ret = BundledLinearEncoder.verify_code_config(config_lin, **config_code)
        return ret

    def __str__(self):
        return "CWC: S = %d\n" \
               "     N = %d ** %d\n" \
               "     W = %d\n" \
               "     K = %d" % (self.params.S, self.params.N_base, self.params.N_exp,
                                self.params.W, self.params.K)


class EncoderCWC(BaseEncoder, BaseCWC):
    """
    Wrapper for CWC encoder

    Provides CWC encoding functionality based generated codebook (look-up table)
    based on the desired CWC construction.
    """

    NUMPY_DATATYPE = np.uint8

    def __init__(self, p: int, config: ConfigCWC, codebook: bool = True, k_i: int = None):
        """
        :param p: used prime field in msg
        :param config: specifies config for CWC
        :param codebook: states if codebook should be constructed (not necessary for KPI estimation)
        :return: EncoderOOC object
        """
        self.p = p
        self.codebook = None
        if config == ConfigCWC.OOC:
            if codebook:
                self.codebook = self.__extend_to_binary(p, self.__encode_ooc(p))
            self.params = BaseCWC.Params(
                S=p * (p - 1), N_base=p, N_exp=1, W=p - 1, K=0
            )
        elif config == ConfigCWC.PPM:
            if codebook:
                self.codebook = np.identity(p, dtype=self.NUMPY_DATATYPE)
            self.params = BaseCWC.Params(
                S=p, N_base=p, N_exp=1, W=1, K=0
            )
        elif config == ConfigCWC.HCWC and k_i:
            self.params = BaseCWC.Params(
                S=p ** 2, N_base=p, N_exp=k_i, W=p, K=k_i - 1
            )

    def encode(self, symbol: int, cue: int = None, prev_cue: int = 0):
        """
        Encodes a single symbol of field size p to a full size constant weight
        codeword. Afterwards either the symbol (1 or 0) is returned if cue is
        given or a cue for a randomly selected 1 is returned.

        !!! ATTENTION !!!
        If `cue` is given, it must be within the range of the codeword.
        -> apply modulo calculations to the full codeword cue

        :param symbol: symbol that will be encoded
        :param cue: position of the codeword symbol that should be determined
        :param prev_cue: cue from previous encoder
        :return: CodewordID (cue, symbol) tuple
        """
        cw_symbol = 1
        cw_cue = cue
        word_cwc = self.codebook[symbol]
        if cue is None:
            cw_cue = int(np.random.choice(np.where(word_cwc == 1)[0], 1))
        else:
            if not cue < self.length():
                raise CueLengthException
            cw_symbol = word_cwc[cue]
        cw_cue += prev_cue * self.length()
        return super(EncoderCWC, self).CodewordID(symbol=cw_symbol, cue=cw_cue)

    def get_code_params(self):
        """
        :return: relevant code parameter for plotting
        """
        return dict(p=self.p)

    @staticmethod
    def __encode_ooc(p: int):
        """
        Creates codebook for OOC code.

        :return: np.ndarray with corresponding codeword to every input symbol
        """
        codebook_ooc = np.zeros([p, p - 1], dtype=EncoderCWC.NUMPY_DATATYPE)
        for i in range(1, codebook_ooc.shape[0]):
            word = np.array([((j * i) % p) for j in range(p)])
            word = np.delete(word, 0)
            codebook_ooc[i] = word
        return codebook_ooc

    @staticmethod
    def __extend_to_binary(p: int, codebook: np.ndarray):
        """
        Creates codebook for PPM code.

        :param codebook: codebook to be encoded with PPM code
        :return: codebook (np.ndarray) with corresponding codeword to every input symbol
        """
        codebook_ext = np.zeros((codebook.shape[0], codebook.shape[1] * p), dtype=np.uint8)
        one_hot_set = np.identity(p, dtype=np.uint8)
        for i in range(codebook.shape[0]):
            codebook_ext[i, :] = one_hot_set[codebook[i, :]].flatten()
        return codebook_ext


class BundledLinearEncoder(BaseEncoder, BaseBlockCode):
    """
    Wrapper for linear block codes that are part of the ID CWC.

    This wrapper allows to abstract the concatenated outer linear block code.
    This abstraction allows to treat every outer linear code equally with
    methods that consider the inner construction of the linear block code.
    """

    def __init__(
            self, p: int, config: ConfigBundledLinear, rs_ext: RSExtension = RSExtension.NONE,
            k: int = None,  # RS encoder / No Code
            k_i: int = None, k_o: int = None,  # RS2 encoder
            r: int = None, m: int = None,  # RM encoder
    ):
        """
        :param p: symbol size
        :param config: linear block code config
        :param rs_ext: RS extension (optional)
        :param k: dimension of RS code and NoCode
        :param k_i: inner dimension of RS2 code
        :param k_o: outer dimension of RS2 code
        :param r: order of RM code
        :param m: m-parameter of RM code
        """
        self.config = config
        if config == ConfigBundledLinear.RS:
            self.encoder = [
                EncoderRS(p=p, k=k, rs_extension=rs_ext)
            ]
        if config == ConfigBundledLinear.RS2:
            # Important: start list with the outer encoder
            self.encoder = [
                EncoderRS(p=p, m=k_i, k=k_o, rs_extension=rs_ext),  # outer RS
                EncoderRS(p=p, k=k_i, rs_extension=rs_ext)  # inner RS
            ]
        if config == ConfigBundledLinear.RM:
            self.encoder = [
                EncoderRM(p=p, r=r, m=m)
            ]
        if config == ConfigBundledLinear.NOCODE:
            self.encoder = [
                EncoderNoCode(p=p, k=k)
            ]

    def encode(self, symbol: int, cue: int = None, **kwargs):
        """
        Encoding method for linear block code.

        This method implements the different encoding routines for the different
        linear block codes that are defined.

        :param symbol: ID msg
        :param cue: position of the codeword symbol that should be determined (random if None)
        :return: CodewordID (cue, symbol) tuple
        """
        cue_enc = self.__determine_cue(cue)
        # RS encoding
        if self.config == ConfigBundledLinear.RS:
            symbols = self.encoder[0].field.convert_number(symbol,
                                                           FiniteField.FieldElRepr.INT,
                                                           FiniteField.FieldElRepr.POLYNOMIAL_LIST,
                                                           padding=True)
            return self.encoder[0].encode(symbols, cue_enc[0])
        # RS2 encoding
        if self.config == ConfigBundledLinear.RS2:
            symbols = self.encoder[0].field.convert_number(symbol,
                                                           FiniteField.FieldElRepr.INT,
                                                           FiniteField.FieldElRepr.POLYNOMIAL_LIST)
            codeword = self.encoder[0].encode(symbols, cue_enc[0])
            # field expansion into base field
            # if codeword.symbol is 0 -> padding must be added to list
            symbols = self.encoder[1].field.convert_number(codeword.symbol,
                                                           FiniteField.FieldElRepr.POLYNOMIAL,
                                                           FiniteField.FieldElRepr.BASEFIELD_LIST)
            return self.encoder[1].encode(symbols, cue=cue_enc[1], prev_cue=cue_enc[0])
        # RM encoding
        if self.config == ConfigBundledLinear.RM:
            symbols = self.encoder[0].field.convert_number(symbol,
                                                           FiniteField.FieldElRepr.INT,
                                                           FiniteField.FieldElRepr.POLYNOMIAL_LIST,
                                                           padding=True)
            return self.encoder[0].encode(symbols, cue_enc[0])
        # No Code encoding
        if self.config == ConfigBundledLinear.NOCODE:
            symbols = self.encoder[0].field.convert_number(symbol,
                                                           FiniteField.FieldElRepr.INT,
                                                           FiniteField.FieldElRepr.INT_LIST,
                                                           padding=True)
            return self.encoder[0].encode(symbols, cue_enc[0])

    def encode_full_id(self, id_tx):
        """
        Encodes ID msg. to full length codeword.

        :param id_tx: ID msg.
        :return: list of codewords [(tag, position)-tuples]
        """
        cue_cnt = 1
        for enc in self.encoder:
            cue_cnt *= enc.length()

        cw_list = list()
        for cue in range(cue_cnt):
            cw_list.append(self.encode(id_tx, cue))
        return cw_list

    def __determine_cue(self, cue: int or None):
        """
        Determine cue for each encoder.

        :param cue: cue of the full codeword
        :return: list of cues for each encoder
        """
        # obtain random cue
        if cue is None:
            return [random.randrange(enc.length()) for enc in self.encoder]
        # calculate cue for every encoder
        cue_enc = [cue] * len(self.encoder)
        if len(self.encoder) > 1:
            for i in range(len(self.encoder) - 1):
                len_following_codes = math.prod(list(map(lambda c: c.length(), self.encoder[i + 1:])))
                cue_enc[i], cue_enc[i + 1] = divmod(cue_enc[i], len_following_codes)
        return cue_enc

    def get_code_params(self):
        if self.config == ConfigBundledLinear.RS:
            return self.encoder[0].get_code_params()
        if self.config == ConfigBundledLinear.RS2:
            return dict(
                k_i=self.encoder[1].get_code_params()['k'],
                k_o=self.encoder[0].get_code_params()['k'],
            )
        if self.config == ConfigBundledLinear.RM:
            return self.encoder[0].get_code_params()
        if self.config == ConfigBundledLinear.NOCODE:
            return self.encoder[0].get_code_params()

    @staticmethod
    def required_code_params(config):
        """
        Required code parameters for checking validity.

        :param config: Linear code config
        :return: list with required parameters
        """
        if config == ConfigBundledLinear.RS:
            return ['k']
        if config == ConfigBundledLinear.RS2:
            return ['k_i', 'k_o']
        if config == ConfigBundledLinear.RM:
            return ['r', 'm']
        if config == ConfigBundledLinear.NOCODE:
            return ['k']

    @staticmethod
    def verify_code_config(config, p: int, k: int = None, k_i: int = None,
                           k_o: int = None, r: int = None, m: int = None):
        """
        Verifies code parameters for correct configuration

        :param config: Linear code config
        :param p: symbol size
        :param k: dimension of RS code
        :param k_i: inner dimension of RS2 code
        :param k_o: outer dimension of RS2 code
        :param r: order of RM code
        :param m: m-parameter of RM code
        :return: true if valid parameters
        """
        if config == ConfigBundledLinear.RS:
            if not EncoderRS.verify_code_config(p=p, m=1, k=k):
                return False
        if config == ConfigBundledLinear.RS2:
            if not k_i < k_o:
                return False
            if not EncoderRS.verify_code_config(p=p, m=1, k=k_i):
                return False
            if not EncoderRS.verify_code_config(p=p, m=k_i, k=k_o):
                return False
        if config == ConfigBundledLinear.RM:
            if not EncoderRM.verify_code_config(p=p, r=r, m=m):
                return False
        if config == ConfigBundledLinear.NOCODE:
            if not EncoderNoCode.verify_code_config(p=p, k=k):
                return False
        return True

    def length(self):
        """
        :return: length of concatenated code (codeword vector length).
        """
        return math.prod(list(map(lambda c: c.length(), self.encoder)))

    def dimension(self):
        """
        :return: dimension of concatenated code (message vector length).
        """
        return math.prod(list(map(lambda c: c.dimension(), self.encoder)))


class EncoderRS(BaseEncoder, BaseLinearBlockCode):
    """
    Class for optimized RS encoder that uses functional encoding.
    """

    def __init__(self, p: int, m: int = 1, k: int = 1, rs_extension: RSExtension = RSExtension.NONE):
        """
        :param p: prime for finite (base) field
        :param m: degree of extension field (q = p^m)
        :param k: dimension of code (information word length)
        :param rs_extension: specifies RS extension (default: None).
        """
        self.q = p ** m
        if not EncoderRS.verify_code_config(p=p, m=m, k=k):
            raise IdCodeParameterException
        self.params = BaseLinearBlockCode.Params(
            n=self.q - 1 + rs_extension.value,
            k=k,
            d=self.q - 1 + rs_extension.value - k + 1
        )
        self.field = FiniteField(self.q, self.dimension())

    def encode(self, symbols: list, cue: int, prev_cue: int = 0):
        """
        Encodes word with RS encoder.

        Return tuple of a given position in the codeword (cue) and its corresponding symbol.
        This encoding scheme follows the DFT encoding given in Bossert 2013 theorem 3.1

        !!! ATTENTION !!!
        If `cue` is given, it must be within the range of the codeword.
        -> apply modulo calculations to the full codeword cue

        :param symbols: info word for the encoder
        :param cue: position of the codeword symbol that should be determined
        :param prev_cue: cue from previous encoder
        :return: CodewordID (cue, symbol) tuple
        """
        if not self.length() + 1 - self.q == RSExtension.NONE.value:
            raise IdCodeParameterException
        if not cue < self.length():
            raise CueLengthException
        cue += prev_cue * self.length()
        # handle empty lists (symbol == 0)
        if not len(symbols):
            return EncoderRS.CodewordID(cue=cue, symbol=0)
        # DFT encoding
        alpha = self.field.a ** cue
        symbol = symbols[0]
        for j in range(1, len(symbols)):
            symbol += symbols[j] * alpha ** j

        return EncoderRS.CodewordID(cue=cue, symbol=symbol)

    def get_code_params(self):
        """
        :return: relevant code parameter for plotting
        """
        return dict(k=self.params.k)

    @staticmethod
    def verify_code_config(p, m, k):
        """
        Verifies correct parameters

        :param p: prime for finite (base) field
        :param m: degree of extension field (q = p^m)
        :param k: dimension of code (information word length)
        :return: true if valid
        """
        if not is_prime(p):
            return False
        if not m > 0:
            return False
        if not k < (p ** m):
            return False
        return True


class EncoderRM(BaseEncoder, BaseLinearBlockCode):
    """
    Class for optimized RM encoder.
    """

    def __init__(self, p: int, r: int, m: int):
        """
        :param p: prime for finite field
        :param r: order RM code
        :param m: variable count for RM code
        """
        self.q = p
        self.r = r  # order RM code
        self.m = m  # variable count RM code
        if not EncoderRM.verify_code_config(p=p, r=r, m=m):
            raise IdCodeParameterException
        self.params = BaseLinearBlockCode.Params(
            n=self.q ** m,
            k=math.comb(r + m, m),
            d=(self.q - r) * self.q ** (m - 1)
        )
        self.field = FiniteField(self.q, self.dimension())
        self.cue_field = FiniteField(self.q, m)

    def encode(self, symbols: list, cue: int, prev_cue: int = 0):
        """
        Encodes word with RM encoder.

        Return tuple of a given position in the codeword (cue) and its corresponding symbol.
        This encoding scheme follows the polynomial evaluation developed by Spandri et al. (2021)
        -> basic algorithm taken from TUM script.

        !!! ATTENTION !!!
        If `cue` is given, it must be within the range of the codeword.
        -> apply modulo calculations to the full codeword cue

        :param symbols: info word for the encoder
        :param cue: position of the codeword symbol that should be determined
        :param prev_cue: cue from previous encoder
        :return: CodewordID (cue, symbol) tuple
        """
        if not cue < self.length():
            raise CueLengthException
        cue_poly = self.cue_field.convert_number(cue,
                                                 FiniteField.FieldElRepr.INT,
                                                 FiniteField.FieldElRepr.POLYNOMIAL_LIST,
                                                 padding=True)
        symbol = EncoderRM.__recursive_polynomial(cue_poly, iter(symbols), self.m, self.r)
        return EncoderRM.CodewordID(cue=cue, symbol=symbol)

    @staticmethod
    def __recursive_polynomial(variables, coeffs, n_vars, deg):
        """
        Required for efficient encoding -> taken from TUM script
        """
        if deg == 0 or n_vars == 0:
            return next(coeffs)
        output = 0
        for i in range(deg + 1):
            output += variables[-n_vars] ** i * EncoderRM.__recursive_polynomial(variables, coeffs,
                                                                                 n_vars - 1, deg - i)
        return output

    def get_code_params(self):
        """
        :return: relevant code parameter for plotting
        """
        return dict(
            r=self.r,
            m=self.m,
        )

    @staticmethod
    def verify_code_config(p: int, r: int, m: int):
        """
        Verifies correct parameters.

        :param p: symbol size
        :param r: order of RM code
        :param m: m-parameter of RM code
        :return: true if valid
        """
        if not is_prime(p):
            return False
        if not r > 0:
            return False
        if not m <= r:
            return False
        return True


class EncoderNoCode(BaseEncoder, BaseLinearBlockCode):
    """
    Encoder class for No-Code Encoder
    """

    def get_code_params(self):
        return dict(k=self.params.k)

    def __init__(self, p: int, m: int = 1, k: int = 1):
        self.q = p ** m
        if not EncoderNoCode.verify_code_config(p=p, k=k):
            raise IdCodeParameterException
        self.params = BaseLinearBlockCode.Params(n=k, k=k, d=1)
        self.field = FiniteField(self.q, self.dimension())

    def encode(self, symbols: list, cue: int, **kwargs):
        if not cue < self.length():
            raise CueLengthException
        return EncoderNoCode.CodewordID(cue=cue, symbol=symbols[cue])

    @staticmethod
    def verify_code_config(p: int, k: int):
        if not is_prime(p):
            return False
        if not k > 0:
            return True
        return True


if __name__ == '__main__':
    code = Code(31, ConfigCWC.OOC, ConfigBundledLinear.RM,
                k_i=2, k_o=4, r=1, m=4)
    print(code.params)
