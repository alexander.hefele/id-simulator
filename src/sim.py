import collections
import os
import time
from multiprocessing import Process, Queue

import numpy as np
import pandas as pd
import psutil

from src.comm import Source, Sink
from src.encoder import Code
from src.utils.helper import ResultID, ConfigCWC, ConfigBundledLinear, PATH_DATA


SimRunSettings = collections.namedtuple('SimRunSettings',
                                        ['sum_run_res', 'iterate_cues', 'id_as_str'],
                                        defaults=[False, False, False])
ScenarioFlags = collections.namedtuple('ScenarioFlags',
                                       ['random_per_round'],
                                       defaults=[False])


class Simulation:
    """
    Class for spawning simulations executed on multiple cores.

    The method `run()` initiates the full simulation ran on multiple processes.
    In order to run simulate on the already uses python thread use `run_id()`.
    """

    DIR_SIM_RES = os.path.join(PATH_DATA, 'sim')
    FILE_SIM_RES = os.path.join(DIR_SIM_RES, 'res.csv')

    PANDAS_INT = np.uint64
    PANDAS_FALLBACK = np.nan

    def __init__(self, p: int, config_cwc: ConfigCWC, config_lin: ConfigBundledLinear,
                 run_settings: SimRunSettings = SimRunSettings(),
                 scenario_flags: ScenarioFlags = ScenarioFlags(),
                 **param_lin):
        """
        Initializes simulation object with (sink, source) pair.

        :param p: prime
        :param k_i: dimension of inner RS encoder
        :param k_o: dimension of outer RS encoder
        """
        self.run_settings = run_settings
        self.scenario_flags = scenario_flags
        self.code = Code(p, config_cwc, config_lin, **param_lin)
        self.source = Source(*self.code.encoder)
        self.sink = Sink(*self.code.encoder)
        self.max_id = self.source.max_id

        if not os.path.exists(Simulation.DIR_SIM_RES):
            os.makedirs(Simulation.DIR_SIM_RES)

    def run(self, rounds: int = 1, id_tx: int = None, id_sub: int = None):
        """
        Wrapper for orchestrating a parallelized simulation.

        After running multiple processes this method collects the results
        of the different instances and adds it to the results file.

        :param rounds: amount of simulation rounds for each process
        :param id_tx: ID to be transmitted (random if None)
        :param id_sub: subscribed ID (random if None)
        :return: results dictionary of this run
        """
        results = self.run_multiproc(rounds, id_tx, id_sub)
        for res in results:
            res.update(self.code.get_code_specs_dict())
        df_results = pd.DataFrame(results)
        if os.path.exists(Simulation.FILE_SIM_RES):
            df_read = pd.read_csv(Simulation.FILE_SIM_RES, index_col=0)
            df_results = pd.concat([df_read, df_results], ignore_index=True)
        df_results.to_csv(Simulation.FILE_SIM_RES, float_format='%.3f')
        print('Saved results to '+str(Simulation.FILE_SIM_RES))
        return results

    def run_multiproc(self, rounds: int, id_tx: int = None,
                      id_sub: int = None, n_proc: int = None):
        """
        Runs simulation in multiprocessing environment.

        The Python multiprocessing library is used to spawn multiple processes
        that each runs an individual simulation. By using the Queue module the
        result of each simulation is retrieved and gathered in a list of dictionaries.

        :param rounds: amount of identification rounds per process
        :param id_tx: ID to be transmitted (random if None)
        :param id_sub: subscribed ID (random if None)
        :param n_proc: amount of processes that will be spawned (if None, the max amount of processes is determined)
        :return: list of dictionaries with results
        """
        if n_proc is None:
            n_proc = psutil.cpu_count()
        result_queue = Queue()
        proc_kwargs = dict(
            rounds=rounds,
            id_tx=id_tx,
            id_sub=id_sub,
            queue=result_queue
        )
        processes = [Process(target=self.run_id, kwargs=proc_kwargs) for i in range(n_proc)]
        for proc in processes:
            proc.start()
        for proc in processes:
            proc.join()
        results = [result_queue.get() for _ in processes]
        if self.run_settings.sum_run_res:
            _results = results.copy()
            results = [results[0]]
            for k in ResultID:
                results[0][k] = sum([d[k] for d in _results])
        for res in results:
            res.update({'rounds': sum([res[key] for key in ResultID])})
        return results

    def run_id(self, rounds: int = None, id_tx: int = None,
               id_sub: int = None, queue: Queue = None):
        """
        Method for running a given amount of identification rounds.

        This method can either be used standalone (`queue=None`) or
        in a multiprocessing environment (`queue=multiprocessing.Queue()`).
        If `id_tx` is specified, this id will be transmitted in every
        round. Otherwise, a random ID within the range of possible ids is
        chosen. The same applies to the subscriptions.

        :param rounds: amount of identification rounds
        :param id_tx: ID to be transmitted (randomly chosen ever round if None, with sub != id)
        :param id_sub: subscribed ID (randomly chosen ever round if None)
        :param queue: Queue object in case of multiprocessing
        :return: either Queue (if using multiprocessing) or result dictionary
        """
        res = {key: 0 for key in ResultID}
        if id_tx is None:
            id_tx = self.get_random_id()
        if id_sub is None:
            id_sub = self.sink.get_random_id()
        self.sink.subscribe(id_sub)
        if self.run_settings.iterate_cues:
            rounds = self.sink.lin_enc.length()
        t_start = time.time()
        # different run settings
        if self.run_settings.iterate_cues:
            for cw in self.sink.encode_full_id(id_tx):
                res[self._hypothesis_test(id_tx, self._comm_round_noiseless(id_tx, cw.cue))] += 1
        else:  # default
            for _ in range(rounds):
                if self.scenario_flags.random_per_round:
                    id_tx = self.get_random_id()
                    id_sub = self.sink.get_random_id()
                    self.sink.subscribe(id_sub)
                # run communication round
                res[self._hypothesis_test(id_tx, self._comm_round_noiseless(id_tx))] += 1
        res.update(dict(duration=time.time() - t_start))
        # only add to result dict if deterministic values are used
        if not self.scenario_flags.random_per_round:
            res.update(**self.__convert_id_tx_sub(id_tx, id_sub))

        if queue is None:
            return res
        queue.put(res)

    def _comm_round_noiseless(self, id_tx: int, cue: int = None):
        """
        Emulates a noiseless point-to-point transmission.

        This method wraps the cue generation at the source and
        the verification at the sink based on the subscribed ID
        that was provided when instantiating the sink.

        :param id_tx: ID to be transmitted.
        :param cue: Symbol position that should be used for ID (random if None)
        :return: list of positive identified subscriptions
        """
        if cue is None:
            cue = self.source.generate_cue(id_tx)
        return self.sink.verify_cue(cue)

    def _hypothesis_test(self, id_tx: int, pos_id_subs: list):
        """
        Runs hypothesis test on the received data based on the sent ID.

        :param id_tx: sent ID
        :param pos_id_subs: set of positive identified subscriptions.
        :return: hypothesis test result given in ResultID enum.
        """
        assert type(id_tx) is int
        assert not len(pos_id_subs) > self.sink.SINK_SUBS_MAX
        if len(pos_id_subs) == 0:
            if id_tx in self.sink.subs:
                return ResultID.HYP_FALSE_NEG
            else:
                return ResultID.HYP_NEG
        elif len(pos_id_subs) == 1:
            if id_tx in pos_id_subs:
                return ResultID.HYP_POS
            else:
                return ResultID.HYP_FALSE_POS

    def get_random_id(self):
        """
        Determines random ID at the source.

        This determination assures to be in the range of available IDs
        and is different from the subscribed ID at the sink.

        :return: randomly determined ID
        """
        _id_tx = self.source.get_random_id()
        while _id_tx in self.sink.subs:
            _id_tx = self.source.get_random_id()
        return _id_tx

    def __id_fits_df_dtype(self):
        """
        Checks if ID can be represented as a pandas integer for saving
        in a csv.
        """
        return self.source.max_id <= np.iinfo(Simulation.PANDAS_INT).max

    def __convert_id_tx_sub(self, id_tx, id_sub):
        """
        Converts sent and subscribed ID from the python native data type
        to a pandas data type

        :param id_tx: sent ID
        :param id_sub: subscribed ID
        :return: dictionary with converted IDs
        """
        if self.run_settings.id_as_str:
            return dict(id_tx=str(id_tx),
                        id_sub=str(id_sub))
        if self.__id_fits_df_dtype():
            return dict(id_tx=Simulation.PANDAS_INT(id_tx),
                        id_sub=Simulation.PANDAS_INT(id_sub))
        return dict(id_tx=Simulation.PANDAS_FALLBACK,
                    id_sub=Simulation.PANDAS_FALLBACK)


def sanity_check_file():
    f_path = '/home/alexander/Projects/id-sim/data-remote/sim/res_rs2_all_cues.csv'

    df = pd.read_csv(f_path, index_col=0).reset_index(drop=True)
    df = df.head(500)
    assert len(df['p'].unique()) == 1
    p = int(df['p'].unique()[0])
    assert len(df['k_i'].unique()) == 1
    k_i = int(df['k_i'].unique()[0])
    assert len(df['k_o'].unique()) == 1
    k_o = int(df['k_o'].unique()[0])

    simulation = Simulation(p=p,
                            config_cwc=ConfigCWC.PPM,
                            config_lin=ConfigBundledLinear.RS2,
                            run_settings=SimRunSettings(
                                sum_run_res=False,
                                iterate_cues=True,
                                id_as_str=True,
                            ),
                            scenario_flags=ScenarioFlags(
                                random_per_round=False,
                            ),
                            k_i=k_i,
                            k_o=k_o)

    res_list = list()
    for _, row in df.iterrows():
        res = simulation.run_id(id_tx=int(row['id_tx']), id_sub=int(row['id_sub']))
        res_list.append(res)
    res_df = pd.DataFrame(res_list)

    if df[ResultID.HYP_FALSE_POS].equals(res_df[ResultID.HYP_FALSE_POS]):
        print('Sanity check passed')
    else:
        print('Sanity check FAILED')


if __name__ == '__main__':
    print('### Started in local development settings ###')
    simulation = Simulation(p=13,
                            config_cwc=ConfigCWC.PPM,
                            config_lin=ConfigBundledLinear.RM,
                            run_settings=SimRunSettings(
                                sum_run_res=False,
                                iterate_cues=True,
                                id_as_str=True,
                            ),
                            r=4,
                            m=3)
    simulation.scenario_flags = ScenarioFlags(random_per_round=False)
    res = simulation.run_id(1)
    print(res)
