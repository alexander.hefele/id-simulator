import abc
import random

from src.encoder import EncoderCWC, BundledLinearEncoder, Code
from src.utils.exceptions import IdException, IdCodeException
from src.utils.helper import ConfigCWC, ConfigBundledLinear


class Node(abc.ABC):
    """
    Base class for source and sink nodes.

    This class provides basic encoding functionality based on the given encoders.
    """

    def __init__(self, cwc: EncoderCWC, lin_enc: BundledLinearEncoder):
        """
        Initializes shared class variables. Should be extended in child class.

        :param cwc: CWC encoder object
        :param lin_enc: bundled linear encoder object
        """
        if cwc.codebook is None:
            raise IdCodeException
        self.cwc = cwc
        self.lin_enc = lin_enc
        self.max_id = cwc.p ** lin_enc.dimension()

    def encode(self, id_tx: int = None, cue: int = None):
        """
        Encodes given ID into encoded symbol.

        When cue is given, symbol at that given position is calculated.

        :param id_tx: ID as an Integer within the range of possible IDs (random if None)
        :param cue: optional position of encoded symbol (random if None)
        :return: Encoded symbol
        """
        # determine cue for each encoder if given
        cue_enc, cue_cwc = None, None
        if cue is not None:
            cue_enc, cue_cwc = divmod(cue, self.cwc.length())
        if id_tx is None:
            id_tx = self.get_random_id()
        else:
            if not (0 < id_tx < self.max_id):
                raise IdException('Selected ID not in range.')
        # encode iteratively
        cw = self.lin_enc.encode(symbol=id_tx, cue=cue_enc)
        cw = self.cwc.encode(symbol=int(cw.symbol), cue=cue_cwc, prev_cue=cw.cue)
        return cw

    def encode_full_id(self, id_tx: int):
        """
        Encodes ID msg. to full length codeword.

        :param id_tx: ID msg.
        :return: list of codewords [(tag, position)-tuples]
        """
        if not (0 < id_tx < self.max_id):
            raise IdException('Selected ID not in range.')

        cw_list = list()
        for cw in self.lin_enc.encode_full_id(id_tx):
            cw_list.append(self.cwc.encode(symbol=int(cw.symbol), prev_cue=cw.cue))
        return cw_list

    def get_random_id(self):
        """
        Returns random id within the range of possible IDs based on `k_o`, `k_i` and `q`.
        """
        return random.randrange(1, self.max_id)


class Source(Node):
    """
    Implementation of a source node.

    This class is able to send messages by encoding IDs and transmitting an index based on
    the encoding scheme in Günlü et al. (2021) [Construction 1]
    """

    def __init__(self, cwc: EncoderCWC, lin_enc: BundledLinearEncoder):
        """
        Initializes class variables.

        :param cwc: Bundled CWC encoder object
        :param lin_enc: Bundled linear encoder object
        """
        super(Source, self).__init__(cwc, lin_enc)

    def generate_cue(self, id_tx: int = None, cue: int = None):
        """
        Encodes given ID into a position of a respected 1 in codeword.

        First a concatenated RS code with base conversion is applied. After
        encoding the respected list of symbols, a random symbol is drawn from
        the encoded symbol vector and the position is saved.
        Secondly, the OOC code is applied to the drawn symbol and the final
        random position of the 1 in the codeword is determined.

        :param id_tx: ID as an Integer within the range of possible IDs (random if None)
        :param cue: Cue that should be used for ID (random if None)
        :return: Random position of 1 in codeword as cue
        """
        return self.encode(id_tx, cue).cue


class Sink(Node):
    """
    Implementation of a sink node.

    This class is able to subscribe to specific IDs (currently just one!).
    Based on those subscriptions a hypothesis test can be applied when receiving as
    message whether the received ID is the one the sink subscribed to or not.
    """

    SINK_SUBS_MAX = 1

    def __init__(self, cwc: EncoderCWC, lin_enc: BundledLinearEncoder):
        """
        Initializes class variables.

        :param cwc: Bundled CWC encoder object
        :param lin_enc: Bundled linear encoder object
        """
        super(Sink, self).__init__(cwc, lin_enc)
        self.subs = [self.get_random_id()]

    def verify_cue(self, cue: int):
        """
        Decodes received cue and gives positive identified IDs.

        :param cue: cue transmitted via channel
        :return: list of IDs that were decoded by sink.
        """
        return [sub for sub in self.subs if self.encode(id_tx=sub, cue=cue).symbol == 1]

    def subscribe(self, id_sub: int = None):
        """
        Sink node subscribes to given ID.

        The term "subscribed ID" is used for the ID the sink runs a hypothesis test for.

        :param id_sub: ID the sink subscribes to. (Random ID chosen if None)
        :return: subscribed IDs
        """
        if id_sub is None:
            id_sub = self.get_random_id()
        self.subs = [id_sub]


if __name__ == '__main__':
    code = Code(23, ConfigCWC.OOC, ConfigBundledLinear.RS2, k_i=2, k_o=4)
    source, sink = Source(*code.encoder), Sink(*code.encoder)

    source.encode()
