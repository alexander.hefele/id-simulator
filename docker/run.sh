#!/bin/bash

DOCKER_HOME="/root"
RUN_FLAG=""

function usage() {
	echo "Usage: ./run.sh [OPTIONS]"
	echo
	echo "A wrapper for running ID simulations in a docker container."
	echo
	echo "Options:"
	echo "  -d    Running container detached."
	echo "  -i    Running container interactively."
	echo
}

if [ "${1}" != "-d" ] && [ "${1}" != "-i" ]; then
	echo "flag not supported"
	echo
	usage
	exit
else
	RUN_FLAG="${1}"
fi

if [ ! -d data-docker ]
then
    mkdir -p data-docker
fi

docker rm IdSim 2>/dev/null || true

docker run "${RUN_FLAG}" \
	--tty \
	--mount type=bind,readonly,source="$(pwd)"/run_sim.sh,target=${DOCKER_HOME}/run_sim.sh \
	--mount type=bind,readonly,source="$(pwd)"/main.py,target=${DOCKER_HOME}/main.py \
	--mount type=bind,readonly,source="$(pwd)"/src,target=${DOCKER_HOME}/src \
	--mount type=bind,source="$(pwd)"/data-docker,target=${DOCKER_HOME}/data \
	--name IdSim \
	id-sim

if [ "${RUN_FLAG}" == "-d" ]; then
	echo ""
	docker logs -tf IdSim
fi
