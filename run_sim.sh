#!/usr/bin/env bash

# To start script remotely detached:
#	1. log in via SSH
#	2. open tmux shell
#	3. run script
#	4. detach with ctrl+b, d
# Reattach with: $ tmux attach -t 0

PATH_SIM="data/sim"
FILE_RES="${PATH_SIM}/res.csv"
FILE_RES_ACC="${PATH_SIM}/res_acc.csv"

: > ${FILE_RES_ACC}

[ -e ${FILE_RES} ] && rm ${FILE_RES}

function random_pairs() {
	echo "--- Run random pairs with (p * ${rnd_factor}) rounds and ${runs} runs ---"
	scenario="random-pairs"
	run_test ${scenario}
	f_res="res_${scenario}_${cwc}_${lin}_${1}.csv"
	cp ${FILE_RES} "${PATH_SIM}/${f_res}" && echo "# Scenario finished: results in  ${f_res}."
	sed -i '1d' ${FILE_RES}
	cat ${FILE_RES} >> ${FILE_RES_ACC}
	rm ${FILE_RES}
	echo
}

function deterministic_sub() {
	echo "--- Run deterministic subscription with (p * ${rnd_factor}) rounds and ${runs} runs ---"
	scenario="deterministic-sub"
	run_test ${scenario}
	f_res="res_${scenario}_${cwc}_${lin}.csv"
	mv ${FILE_RES} "${PATH_SIM}/${f_res}" && echo "# Scenario finished: results in  ${f_res}."
	echo
}

function deterministic_pairs() {
	echo "--- Run deterministic pairs with (p * ${rnd_factor}) rounds and ${runs} runs ---"
	scenario="deterministic-pairs"
	run_test ${scenario}
	f_res="res_${scenario}_${cwc}_${lin}.csv"
	mv ${FILE_RES} "${PATH_SIM}/${f_res}" && echo "# Scenario finished: results in  ${f_res}."
	echo
}

function run_nocode() {
	scen=${1}
	for p in 61; do
		k=$((p*rnd_factor))
		lin_params="-p ${p} -k ${k}"
		echo -n "  p: ${p} | k: ${k}  >> "
		python main.py ${lin_params} --cwc ${cwc} --lin ${lin} --${scen} --iterate-cues
		echo "done"
	done
}

function run_rs2() {
	scen=${1}
#	for p in 61 251 1021; do
	for p in 13; do
		for k_i in 2; do
			k_o=$p
			lin_params="-p ${p} -k_i ${k_i} -k_o ${k_o}"
			echo -n "  p: ${p} | k_i: ${k_i} | k_o: ${k_o}  >> "
			python main.py ${lin_params} --cwc ${cwc} --lin ${lin} --${scen} --iterate-cues --id-tx-as-str
			echo "done"
		done
	done
}

function run_rm() {
	scen=${1}
	m=3

	p=13
	r=4
	lin_params="-p ${p} -m ${m} -r ${r}"
	echo -n "  p: ${p} | m: ${m} | r: ${r}  >> "
	python main.py ${lin_params} --cwc ${cwc} --lin ${lin} --${scen} --iterate-cues --id-tx-as-str
	echo "done"
}

function run_test() {
	scen=${1}
	for i_run in $(seq ${runs}); do
		echo "# run: ${i_run}"
		t_run_start=$(date +%s.%N)

		if [[ "${lin}" == "NoCode" ]]; then
			run_nocode $scen
		fi
		if [[ "${lin}" == "rs2" ]]; then
			run_rs2 $scen
		fi
		if [[ "${lin}" == "rm" ]]; then
			run_rm $scen
		fi

		t_run_end=$(date +%s.%N)
		t_run="$( echo "${t_run_end} - ${t_run_start}" | bc -l )"
		echo "  run took ${t_run} sec."
	done
}

# settings
rnd_factor=100
hyperruns=10
runs=625  # yields 15000 / 30000 runs

lin="rs2"
cwc="ppm"

for i in $(seq ${hyperruns}); do
	random_pairs $i
done
