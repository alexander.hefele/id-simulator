# Simulation Framework for Point-to-Point Identification

This tool provides methods for running a simulation of a point-to-point communication with a sink and a source with _ID-as-a-service_.

This simulation tool implements several linear error correction codes and CWCs that can be combined to construct an ID code. 
Those codes can either be used for encoding messages or for a KPI estimation of the constructed codes.

| ID encoder // KPI Estimation |  **PPM** [1]| **OOCWC** [5] | **HCWC** [6] |
|:----------------------------:|:----------:|:----------:|:---------:|
|            **RS**            | yes // yes |  no // no  | no // no  |
|           **RS2** [1-3]      | yes // yes | yes // yes | no // yes |
|            **RM**  [4]       | yes // yes |  no // no  | no // no  |
|         **No Code** [9]      | yes // yes |  no // no  | no // no  |

OOCWCs and HCWCs require concatenated outer linear block codes.
Thus, those CWCs can only be used with RS2 codes as the outer liner block code.

The HCWC construction has not been implemented yet. 

## How cite this?

```biblatex
@misc{id_sim,
    author = {Alexander {Hefele} and Caspar {von Lengerke} and Roberto {Ferrara} and Juan A. {Cabrera} and Frank H. P. {Fitzek}},
    title = {{ID-Sim}: A simulation framework for message identification},
    year = {2022},
    publisher = {GitLab},
    journal = {GitLab repository},
    howpublished = {\url{https://gitlab.com/alexander.hefele/id-simulator}},
}
```
We used this framework for publications [7-10].
## Dependencies

This code has a strong dependency on [SageMath](https://www.sagemath.org/).
Thus, it must be installed either locally on your system, a virtual environment or a conda environment.
I would recommend the using the installation with a [conda environment](https://doc.sagemath.org/html/en/installation/conda.html).

For creating the conda environment run the following command:
```shell
    $ conda env create -f env.yml
```

Note that SageMath is provided through _conda-forge_. 
This needs to be set up previously.

For local installation from the source code, check the following [link](https://doc.sagemath.org/html/en/installation/source.html#sec-installation-from-sources).

## Docker Container

This simulation tool ships with a Docker image for easier deployment.
The default container name is `IdSim`.
This can be changed if preferred.

### Building Image

The container image for providing the dependencies for the ID simulation.
You can build the container image by running the following command.

```bash
    docker build -t "id-sim:latest" .
```

Note that the created image has a size of ~9 GB and requires ~2 GB download of dependencies.

### Running Container

For running the container the repository includes an orchestration script at `docker/run.sh`.
The script allows interactive (`-i`) and detached (`-d`) container spinup.

For running the container manually use the following command. 
The three `--mount` flags assure correct mounting of the `run_sim.sh` script as well as the `src` directory.
For a different orchestration script for the simulation you can alter the source path of the first mount flag.
However, the target must remain the same.

```bash
    docker run "${RUN_FLAG}" \
        --tty \
        --mount type=bind,readonly,source="$(pwd)"/run_sim.sh,target=${DOCKER_HOME}/run_sim.sh \
        --mount type=bind,readonly,source="$(pwd)"/src,target=${DOCKER_HOME}/src \
        --mount type=bind,source="$(pwd)"/data-docker,target=${DOCKER_HOME}/data \
        --name IdSim \
        id-sim
```

The results are saved to `data-docker/sim/*`.

### Logging

When running in detached mode the current status of the simulation can be assessed through the logging functionality that is built into docker.

```bash
    $ docker logs -tf IdSim
```

## Simulation Framework

The simulation is invoked by running the python script:

```shell
    $ python src/sim.py
        [-p P]                                          # symbol size
        [-k_i K_I] [-k_o K_O]                           # RS2 parameters
        [-k K]                                          # RS parameters
        [-m M] [-r R]                                   # RM parameters 
        [--cwc CONFIG_CWC] [--lin CONFIG_LIN]           # encoder selection
        [--rounds ROUNDS]                               # simulation round
        [--sanity-check] [--random-pairs] 
        [--deterministic-sub] [--deterministic-pairs]   # scenario flags
        [--sum-run-results]                             # additional simulation flags
```

Assert that the base directory is added to the `PYTHONPATH`. 
The `run_sim.sh` script does that automatically. 
For executing the `main.py` manually use the following command in the base directory of the project:

```shell
    $ export PYTHONPATH="$(pwd)"
```

The scenario flags determines the used scenario described in the section below.
Only one scenario can be selected.

The given `<rounds>` determine the simulation round per CPU core used in the multiprocessing environment.
With setting the `--sum-run-results` flag the results of each simulation process are accumulated. 
This requires using the same ID pair for every spawned process.

When invoking the simulation the data is saved to `data/sim/res.csv`.
This path can be changed by modifying the `DIR_SIM_RES` class attribute in `sim.py`.

When scaling the simulations, I suggest writing a bash script sequentially invoking the simulation with different parameter sets (example given in `run-sim.sh`).
The created `res.csv` file can be renamed and moved accordingly. 
If this is not done, the results will be lost since the following simulation overwrites the file.

### Linear Encoder

The implemented linear encoders share the symbol size `p` (prime number) as a common parameter.
Thus, it is required for a parameters.

#### Reed-Solomon (RS) Encoder | `--lin rs`

The RS encoder has the dimension `k` as an additional required parameter.

#### Concatenated Reed-Solomon (RS2) Encoder | `--lin rs2`

The concatenated RS encoder requires the code parameters `-k_i` and `-k_o` that refer to the inner and outer dimension of the inner and the outer RS code.

#### Reed-Muller (RM) Encoder | `--lin rm`

The RM encoder requires the `-m` and `-r` parameter.

#### NoCode Encoder | `--lin nocode`

The NoCode encoder is a linear block code that does not perform any encoding.
Therefore it is a (k, k, 1)-code where `k` is the message and codeword length.

### CWC Encoder

The CWC encoders only require the symbol `q` for performing the bijective mapping.

Available CWCs are the following
    * PPM
    * OOCWC (requires RS2 as linear code)
    * HCWC (requires RS2 as linear code)


## Scenarios

This simulation tool already includes several scenarios.

### Random Pairs | `--random-pairs`

In this scenario the sent and the subscribed ID are chosen randomly upon every run.
This can be changed to random per round with the scenario flag `random_per_round` in `ìd_sim.py`.
It is made sure that the sent and subscribed ID are not equal (even though it is very unlikely anyway).

-> This scenario reflects the definition the ID problem is designed for with its pairwise error definition.

### Deterministic Subscription | `--deterministic-sub`

In this scenario the subscribed ID is chosen deterministically before starting the identification rounds.
The transmitted ID on the other hand is chosen randomly.
It is once again made sure the subscribed and transmitted ID are not equal.

### Deterministic Pairs | `--deterministic-pairs`

For deterministic pairs the subscribed and transmitted ID are chosen before starting the ID rounds.
Both IDs are ensured not to be equal. 

### Sanity check | `--sanity-check`

This scenario is only used for checking whether the encoders are implemented correctly. 
It selects a common transmitted and subscribed ID. 
The true-positive results should be at 100%. 

## Structure

This section will give a brief overview of the structure used for implementing the simulation.
The relevant files are the following:
  * `comm.py`
  * `encoder.py`
  * `scenarios.py`
  * `sim.py`
  * `utils/helper.py`

As already described in the previous section, `sim.py` provides the simulation functionalities with the possibility of replicating the random identification rounds on a given number of processors.
In order to simulate the different entities of the point-to-point communication system the classes `Sink` and `Source` - both inheriting from the abstract `Node` base class - are instantiated.
Every object inheriting from the `Node` class provides an `encode()` function.
This function utilizes the encoder objects, provided with the instantiation process of the derived object and returns a (cue, symbol) tuple based on a given ID message.
Depending on whether a `cue` is given as a parameter or not the encode function returns either the codeword symbol at the deterministically given codeword position (cue) or it randomly selects a codeword symbol position that is equal to 1.

In `encoder.py` the different codes are implemented. 
For further reading on the functionality check the in-code docs.


## KPI Estimation

Apart from running encoding routines, this framework is also capable of estimating the KPIs of an ID code based on their construction with specific parameters.
Those KPIs include:

  * ID rate
  * ID error exponent ($\lambda_2$)
  * ID error bound 
  * cue size (`n_cue`) 
  * ID size (`n_info`)
  * The parameters of the resulting ID CWC (S, N, W, K)

Primary entry-point in the KPI estimation is the `estimate()` function in `kpi_estimation.py`.
This method takes the different options for CWCs and linear block codes and a range for the symbol size `p`.
It then iterates over a grid of possible code parameters, writing the results in a `*.csv` file.

An alternative to the automated approach with the `estimate()` function is the `CodeEstimation` with its `run()` method.
This method is the core of this class allowing to run a specific code configuration. 
It allows single parameters as well as parameter ranges.

## References
[1] [Verdu, S., & Wei, V. K. (1993). Explicit construction of optimal constant-weight codes for identification via channels. IEEE Transactions on Information Theory, 39(1), 30-36.](https://doi.org/10.1109/18.179339)

[2] [Derebeyoğlu, S., Deppe, C., & Ferrara, R. (2020). Performance analysis of identification codes. Entropy, 22(10), 1067.](https://doi.org/10.3390/e22101067)

[3] [Ferrara, R., Torres-Figueroa, L., Boche, H., Deppe, C., Labidi, W., Moenich, J. U., & Andrei, V. C. (2022, September). Implementation and Experimental Evaluation of Reed-Solomon Identification. In European Wireless 2022; 27th European Wireless Conference (pp. 1-6). VDE.](https://ieeexplore.ieee.org/abstract/document/10071924)

[4] [Spandri, M., Ferrara, R., & Deppe, C. (2021). Reed-muller identification. in Proc. Int. Zurich Seminar on Information and Commun. (IZS), Mar. 2021, pp. 74–78.](https://doi.org/10.3929/ethz-b-000535285)

[5] [Günlü, O., Kliewer, J., Schaefer, R. F., & Sidorenko, V. (2021). Code constructions and bounds for identification via channels. IEEE Transactions on Communications, 70(3), 1486-1496.](https://doi.org/10.1109/TCOMM.2021.3136864)

[6] [Kurosawa, K., & Yoshida, T. (1999). Strongly universal hashing and identification codes via channels. IEEE Transactions on Information Theory, 45(6), 2091-2095.](https://doi.org/10.1109/18.782144)

[7] [von Lengerke, C., Hefele, A., Cabrera, J. A., & Fitzek, F. H. (2022, April). Stopping the data flood: Post-shannon traffic reduction in digital-twins applications. In NOMS 2022-2022 IEEE/IFIP Network Operations and Management Symposium (pp. 1-5). IEEE.](https://doi.org/10.1109/NOMS54207.2022.9789759)

[8] [von Lengerke, C., Hefele, A., Cabrera, J. A., Kosut, O., Reisslein, M., & Fitzek, F. H. (2023). Identification Codes: A Topical Review with Design Guidelines for Practical Systems. IEEE Access.](https://doi.org/10.1109/ACCESS.2023.3244071)

[9] [von Lengerke, C., Hefele, A., Cabrera, J. A., Reisslein, M., & Fitzek, F. H. P., “Beyond the bound: A new performance perspective for identification via channels,” IEEE JSAC.](https://doi.org/10.1109/JSAC.2023.3288239)

[10] [von Lengerke, C., Cabrera, J. A. & Fitzek, F. H. P., “Identification codes for increased reliability in digital twin applications over noisy channels,” in Proc. IEEE MetaCom.](https://doi.org/10.1109/MetaCom57706.2023.00098)
