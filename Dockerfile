FROM condaforge/miniforge3

MAINTAINER Caspar von Lengerke caspar.lengerke@tu-dresden.de

# Environment
ENV SHELL /usr/bin/bash

RUN apt-get update && apt-get install -y \
    bc

ARG HOME=/root
WORKDIR $HOME
ENV PYTHONPATH ${HOME}

RUN mkdir ${HOME}/data

COPY env.yml ./
RUN conda env create -f env.yml
RUN conda init -q bash

RUN echo "conda activate IdSim" >> ~/.bashrc
SHELL ["/usr/bin/bash", "--login", "-c"]

COPY /src ./src/
COPY main.py ./
COPY run_sim.sh ./

ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "IdSim", "./run_sim.sh"]
